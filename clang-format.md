# 前言
这里一直使用cpplint（即谷歌代码风格检查工具），就是代码会提示哪里写错了要修改，[C++google风格指南的中文整理版](https://google-styleguide.readthedocs.io/zh_CN/latest/google-cpp-styleguide/contents.html)，但机器人开发少不了roslint（符合ros的风格的，与google的差不多，但有些差别），就想着两者结合起来用用。
# clang format
[clion配置cpplint教程](https://gitlab.com/gdut-yxj/tutorials/clion-cpplint/-/blob/main/ubuntu%E4%B8%8B%E7%9A%84clion%E9%85%8D%E7%BD%AEcpplint.md)，因为cpplint比较详细又有中文解释，但是它与[roslint规则](http://wiki.ros.org/CppStyleGuide#Autoformatting_of_ROS_Code)有一些不同，主要就是在格式上面（指的是代码的一些布局）。要使两者统一，就要关闭或者开启cpplint的某些提示功能，有一种办法就是找到cpplint.py的源码修改，另一种就是当你熟练掌握后如果你嫌太烦就可以关闭提示，可以直接使用了clang-format，它就是一开始定义好了格式（即哪些检查关闭哪些开启，可以自定义），然后点几个按钮后就可以帮你**自动格式化代码（只是布局哦）**，然后**其食用指南如下：**
[C++里clang-format的一些含义](https://blog.csdn.net/core571/article/details/82867932)
[ROS里使用C++google的clang-format文件下载](https://github.com/PickNikRobotics/roscpp_code_format)
如果要使用clangformat，就把上面的最后一个连接文件下载下来，将`.clang-format`文件放到工作空间下，这里我是`~/rm_ws/`,然后clion里设置画面如下![在这里插入图片描述](https://img-blog.csdnimg.cn/777a6a54f9604c11aca1ff33179a88ad.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBARXlldWk=,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

然后写完代码后，点**代码--自动格式化代码**，这样你的代码就会按照clang-format格式进行拉。
# 要注意的规则
1. cpplint的长度最大为80行，roslint为120行，此处依据**roslint的120行**，偷偷修改了cpplint的提示
![在这里插入图片描述](https://img-blog.csdnimg.cn/3e5eb6ed30204198bd0371837c170ac8.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBARXlldWk=,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

其实也可以在clang format修改

![在这里插入图片描述](https://img-blog.csdnimg.cn/375d00a7ef274836abc2a8ad656e4441.png#pic_center)

2. 头文件的声明里，区别在后面有无下划线，我用的是google的风格，没多大差别
```cpp
roslint:

#ifndef PACKAGE_PATH_FILE_H
#define PACKAGE_PATH_FILE_H
...
#endif

cpplint:

#ifndef FOO_BAR_BAZ_H_
#define FOO_BAR_BAZ_H_
//<PROJECT>_<PATH>_<FILE>_H_
...
#endif // FOO_BAR_BAZ_H_
```
3. 块与块之间用**两个空格**区别开，不可用tab键，在块的开始与关闭里，roslint大括号要沿着开头的一列。cpplint则是一边在右边的。这里我选择用roslint，它看起来比较顺眼
```cpp
if(a < b)
{
  for(int i=0; i<10; i++)
    PrintItem(i);
}
// 只要其中一个分支用了大括号, 两个分支都要用上大括号.
if (condition) {
  foo;
} else {
  bar;
}
```
这个可以在clang format修改的，在这个地方，具体意思可以看上面的链接里。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d24b3b0577894a74b9d667c2ff46fa40.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBARXlldWk=,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

5. 变量命名（这里clang format就不会动到，自己要时刻记住）
+ CamelCased

> 类名
> eg：class ExampleClass;
> class HokuyoURGLaser;这种情况在单词都是大写的时候，就该词统一大写，比如URG


+ camelCased
>函数或者类方法都是该格式，并且参数为under_scored
>int exampleMethod(int example_arg);
+ under_scored

> 1. 包名 
> 2. topic和service名
> 3. cpp，h的名字
> 要记住尽量写详细一点，比如对于laser.cpp,使用 hokuyo_topurg_laser.cpp.
> 最好文件名和类名类似，the class ActionServer在action_server.h
> 4. 库名
> lib_my_great_thing ## Bad          libmy_great_thing ## Good，lib后面不得有下划线
> 5. 普通变量名，但是迭代器可以用i j k
> 6. 类成员
> int example_int_;最后记得带有下划线
> 7. 全局变量
> int g_shutdown; 前面带有g_
> 8. 命名空间
> + 名字可以依据包名
> + 不要使用using namespace xx在头文件里，但是在源文件可以用，只是最好直接声明会用到的
> `using namespace std; // Bad, because it imports all names from std::`
> `using std::list;  // I want to refer to std::list as list`



+ ALL_CAPITALS
>Constants,,wherever they are used, are ALL_CAPITALS.
>枚举
>enum
>{
>STOP,
>READY,
>PUSH,
>BLOCK
>};




后续更新看情况
