# ubuntu下的clion配置cpplint

1. 打开终端，如果没有安装pip3的先安装 `sudo apt install python3-pip`

2. 安装好后，输入`pip3 install cpplint`

3. 安装完成后，出现~/.local/bin/cpplint

   源码位于~/.local/lib/[python](https://so.csdn.net/so/search?from=pc_blog_highlight&q=python)3.8/site-packages/cpplint.py （大致路径长这样的）

4. 在clion里面，文件-设置-插件，搜索cpplint并安装，安装成功后，再点开设置，会出现**cpplint option**选项，在里面python path 和cpplint.py path 配置好后，保存。

   ![2021-11-18 22-29-20 的屏幕截图.png](https://i.loli.net/2021/11/18/jxY5TMEQwp92RbP.png)

5. 重启clion就可以看到啦